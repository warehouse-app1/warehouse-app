import { Component, OnInit } from '@angular/core';
import { ITEM } from 'src/app/models/ITEM';
import { ItemsService } from 'src/app/services/items.service';

@Component({
  selector: 'app-computers',
  templateUrl: './computers.component.html',
  styleUrls: ['./computers.component.css']
})
export class ComputersComponent implements OnInit {

  itemList : ITEM[] = [];
  filteredList: ITEM[] = [];

  constructor(private itm:ItemsService) { }

  ngOnInit(): void {
    this.subscribe();
    this.filter()
  }
  subscribe(){
    this.itm.getItemJson().subscribe(i => this.itemList = i);
  }

  filter(){
    for (let x = 0; x < this.itemList.length; x++){
      if (this.itemList[x].category  == "Computers"){
        this.filteredList.push(this.itemList[x])
      } 
    }
  }


}
