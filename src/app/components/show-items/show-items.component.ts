import { Component, OnInit } from '@angular/core';
import { Item } from '../../models/Items';
import ItemsData from '../../Items.json'

@Component({
  selector: 'app-show-items',
  templateUrl: './show-items.component.html',
  styleUrls: ['./show-items.component.css']
})
export class ShowItemsComponent implements OnInit {

  items: Item[] = ItemsData;

  constructor() { }

  ngOnInit(): void {
  }

}
