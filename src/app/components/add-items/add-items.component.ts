import { Component, OnInit } from '@angular/core';
import { Item } from '../../models/Items';
import ItemsData from '../../Items.json';

@Component({
  selector: 'app-add-items',
  templateUrl: './add-items.component.html',
  styleUrls: ['./add-items.component.css']
})
export class AddItemsComponent implements OnInit {

  newItems: Item[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
