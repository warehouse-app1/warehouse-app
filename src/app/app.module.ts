import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddItemsComponent } from './components/add-items/add-items.component';
import { AppliancesComponent } from './components/display/appliances/appliances.component';
import { ComputersComponent } from './components/display/computers/computers.component';
import { ToysgamesComponent } from './components/display/toysgames/toysgames.component';
import { VideogamesComponent } from './components/display/videogames/videogames.component';
import { ShowItemsComponent } from './components/show-items/show-items.component';


@NgModule({
  declarations: [
    AppComponent,
    AppliancesComponent,
    ComputersComponent,
    ToysgamesComponent,
    VideogamesComponent,
	AddItemsComponent,
    ShowItemsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
