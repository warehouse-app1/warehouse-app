export interface Item {
    name: String,
    category: String,
    desc: String,
    price: Number
}