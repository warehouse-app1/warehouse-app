import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppliancesComponent } from './components/display/appliances/appliances.component';
import { ComputersComponent } from './components/display/computers/computers.component';
import { ToysgamesComponent } from './components/display/toysgames/toysgames.component';
import { VideogamesComponent } from './components/display/videogames/videogames.component';

const routes: Routes = [
  {
    path:"appliances",
    component:AppliancesComponent
  },
  {
    path:"computers",
    component:ComputersComponent
  },
  {
    path:"toysgames",
    component:ToysgamesComponent
  },
  {
    path:"videogames",
    component:VideogamesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
