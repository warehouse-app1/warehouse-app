import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import ItemListJson from '../Items.json'
import { ITEM } from '../models/ITEM';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  itemList : ITEM[] = [];
  constructor() { }

  getItemJson():Observable<ITEM[]>{
    this.itemList = ItemListJson;
   return of (this.itemList)
  }
}
